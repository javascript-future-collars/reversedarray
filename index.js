// let arr = [1, "b", 3, 4, 5, "x"];
let arr = [null, "b", 3, 4, 5, "x"];

const reversedArray = (arr) => {
  return [...arr.reverse()];
};

console.log(reversedArray(arr));
